<?=$this->extend("layout")?>

<?=$this->section("content")?>

<div class="container">
    <div class="row justify-content-md-center mt-5">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Pages</a>
                    <div class="d-flex">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="<?php echo base_url('/dashboard'); ?>">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="<?php echo base_url('/pages'); ?>">Pages</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="<?php echo base_url('/logout'); ?>">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="table-responsive mt-5">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-10">
                                <h2>Pages</h2>
                            </div>
                            <div class="col-sm-2 text-end">
                                <a href="<?php echo base_url('/pages/new'); ?>" class="btn btn-success" data-toggle="modal"><i class="material-icons"></i> <span>Add Page</span></a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pages as $page) { ?>
                                <tr>
                                    <td><?=$page['id']?></td>
                                    <td><?=$page['name']?></td>
                                    <td><?=$page['link']?></td>
                                    <td>
                                        <a href="<?php echo base_url("/pages/{$page['id']}"); ?>" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit"></i></a>
                                        <a href="javascript:;" class="delete" data-id="<?=$page['id']?>"><i class="material-icons" data-toggle="tooltip" title="Delete"></i></a>
                                        <form method="post" id="frmDeletePage<?=$page['id']?>" action="<?= site_url("/pages/{$page['id']}") ?>">
                                            <input type="hidden" name="id" name="id" value="<?=$page['id']?>" />
                                            <input type="hidden" name="_method" name="_method" value="delete" />
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <nav>
                        <?=$pager->links()?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".delete").click(function() {
            if (confirm('Are you sure you want to delete?')) {
                var id = $(this).data('id');
                $("#frmDeletePage" + id).submit();
            }
        })
    });
</script>

<?=$this->endSection()?>

