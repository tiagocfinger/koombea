<?=$this->extend("layout")?>

<?=$this->section("content")?>

<div class="container">
    <div class="row justify-content-md-center mt-5">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Pages</a>
                    <div class="d-flex">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="<?php echo base_url('/dashboard'); ?>">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="<?php echo base_url('/pages'); ?>">Pages</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="<?php echo base_url('/logout'); ?>">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="table-responsive mt-5">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-10">
                                <h2>Add Page</h2>
                            </div>
                        </div>
                    </div>
                    <form method ="post" action="<?= site_url('/pages/' . current_url(true)->getSegment(3)) ?>">
                        <input type="hidden" name="id" name="id" value="<?=current_url(true)->getSegment(3)?>" />
                        <input type="hidden" name="_method" name="_method" value="put" />
                        <div class="content">
                            <div class="form-group mt-3">
                                <label>Name</label>
                                <input type="text" name="name" id="name" value="<?= set_value('name') ? set_value('name') : $data['name'] ?>" class="form-control" required>
                                <?php if(isset($validation)):?>
                                    <small class="text-danger"><?= $validation->getError('name') ?></small>
                                <?php endif;?>
                            </div>
                            <div class="form-group mt-3">
                                <label>Link</label>
                                <input type="text"  name="link" id="link" value="<?= set_value('link') ? set_value('link') : $data['link'] ?>" class="form-control" required>
                                <?php if(isset($validation)):?>
                                    <small class="text-danger"><?= $validation->getError('link') ?></small>
                                <?php endif;?>
                            </div>
                        </div>
                        <h3 class="mt-4">Page Links</h3>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Link</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($pageLinks as $pageLink) { ?>
                                <tr>
                                    <td><?=$pageLink['id']?></td>
                                    <td><?=$pageLink['name']?></td>
                                    <td><?=$pageLink['link']?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <nav>
                            <?=$pagerLinks->links()?>
                        </nav>
                        <div class="footer mt-5 text-end">
                            <a href="<?= site_url('/pages') ?>" class="btn btn-default">Cancel</a>
                            <input type="submit" class="btn btn-info" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->endSection()?>