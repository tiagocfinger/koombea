<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Dashboard extends BaseController
{
    /**
     * @return string
     */
    public function index()
    {
        return view('dashboard');
    }
}
