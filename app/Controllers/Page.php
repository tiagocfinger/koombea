<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PageLinkModel;
use App\Models\PageModel;
use DOMDocument;
use DOMXPath;

class Page extends BaseController
{
    /**
     * @var PageModel
     */
    private PageModel $pageModel;

    /**
     * @var PageLinkModel
     */
    private PageLinkModel $pageLinkModel;

    public function __construct()
    {
        helper(['form']);
        $this->pageModel = new PageModel();
        $this->pageLinkModel = new PageLinkModel();
    }

    /**
     * @return string
     */
    public function index()
    {
        $data = [
            'pages' => $this->pageModel->paginate(20),
            'pager' => $this->pageModel->pager,
        ];

        return view('page', $data);
    }

    /**
     * @return string
     */
    public function new()
    {
        return view('page_new');
    }

    /**
     * @param int $id
     * @return string
     */
    public function edit(int $id)
    {
        $data = [
            'data' => $this->pageModel->find($id),
            'pageLinks' => $this->pageLinkModel->where('page_id', $id)->paginate(20),
            'pagerLinks' => $this->pageLinkModel->pager,
        ];
        return view('page_edit', $data);
    }

    /**
     * @return \CodeIgniter\HTTP\RedirectResponse|string
     * @throws \ReflectionException
     */
    public function store()
    {
        if ($this->validation()) {
            $data = [
                'name' => $this->request->getVar('name'),
                'link' => $this->request->getVar('link'),
                'created_at' => date('y-m-d h:i:s')
            ];
            $this->pageModel->save($data);
            return redirect()->to('/pages');
        }

        $data['validation'] = $this->validator;
        return view('page_new', $data);
    }

    /**
     * @return \CodeIgniter\HTTP\RedirectResponse|string
     * @throws \ReflectionException
     */
    public function update()
    {
        if ($this->validation()) {
            $data = [
                'name' => $this->request->getVar('name'),
                'link' => $this->request->getVar('link'),
                'updated_at' => date('y-m-d h:i:s')
            ];
            $this->pageModel->update($this->request->getVar('id'), $data);
            return redirect()->to("/pages");
        }

        $data = [];
        $data['validation'] = $this->validator;
        return view('page_edit', $data);
    }

    /**
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function delete()
    {
        $id = $this->request->getVar('id');
        $this->pageModel->where('id', $id)
            ->delete($id);
        return redirect()->to("/pages");
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function process()
    {
        $pages = $this->pageModel
            ->where('processed', false)
            ->findAll();

        foreach ($pages as $page) {
            $urlContent = file_get_contents($page['link']);

            $dom = new DOMDocument();
            @$dom->loadHTML($urlContent);

            $xpath = new DOMXPath($dom);
            $hrefs = $xpath->evaluate("/html/body//a");
            $title = $xpath->query('//title')
                ->item(0)
                ->textContent;

            for ($i = 0; $i < $hrefs->length; $i++) {
                $href = $hrefs->item($i);
                $url = filter_var($href->getAttribute('href'), FILTER_SANITIZE_URL);
                $linkContent = strip_tags($href->textContent, '<span><b><strong>');

                // validate url
                if (filter_var($url, FILTER_VALIDATE_URL) && !empty($linkContent)) {
                    $link = '<a href="' . $url . '">' . $linkContent . '</a>';

                    $this->pageLinkModel->save([
                        'page_id' => $page['id'],
                        'link' => $link,
                        'name' => $title,
                        'created_at' => date('y-m-d h:i:s')
                    ]);
                }
            }

            $this->pageModel->update($page['id'], ['processed' => TRUE]);
        }
    }

    /**
     * @return bool
     */
    private function validation()
    {
        $id = current_url(true)->getSegment(3);
        if (!empty($id)) {
            $ruleIsUniqueId = ",id,{$id}";
        } else {
            $ruleIsUniqueId = '';
        }

        $rules = [
            'name' => ['rules' => 'required|min_length[4]|max_length[255]'],
            'link' => [
                'rules' => "required|min_length[8]|max_length[255]|valid_url_strict[http,https]|is_unique[pages.link{$ruleIsUniqueId}]"
            ]
        ];

        if ($this->validate($rules)) {
            return true;
        }

        return false;
    }
}
