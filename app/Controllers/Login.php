<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;

class Login extends BaseController
{
    /**
     * @var UserModel
     */
    private UserModel $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    /**
     * @return string
     */
    public function index()
    {
        return view('login');
    }

    /**
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function authenticate()
    {
        $session = session();

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $user = $this->userModel->where('email', $email)->first();
        if (empty($user)) {
            return redirect()->back()->withInput()->with('error', 'Invalid username or password.');
        }

        $pwdVerify = password_verify($password, $user['password']);
        if (!$pwdVerify) {
            return redirect()->back()->withInput()->with('error', 'Invalid username or password.');
        }

        $sesData = [
            'id' => $user['id'],
            'email' => $user['email'],
            'isLoggedIn' => TRUE
        ];

        $session->set($sesData);
        return redirect()->to('/dashboard');
    }

    /**
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function logout()
    {
        session_destroy();
        return redirect()->to('/login');
    }
}
